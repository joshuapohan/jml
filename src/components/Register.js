import React from 'react';
import $ from 'jquery';

import Header from "./common/Header";
import "./Register.css";

class Register extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            form: {
                username: "",
                email: "",
                password: "",
                passwordConfirmation: "",
                firstName: "",
                lastName: "",
                
                birthDay: "1990-01-01",
            },
            style: {
                pageHeight: 0
            },
            isSubmitted: false,
            errorMsg: ""
        };
        this.updatePageHeight = this.updatePageHeight.bind(this);
    }

    updatePageHeight(){
        let winHeight = $(window).height();
        let navHeight = $("#navbar").height();
        let footHeight = $("#footer").height();
        let pageHeight =  winHeight - (navHeight * 2) - footHeight;
        this.setState({
            style: {
                pageHeight: pageHeight
            }
        });
    }    

    onChange = (e) =>{
        let key = $(e.target).attr("id");
        let newValue = e.target.value;
        if(key === "passwordConfirmation"){
            this.setState(state=>{
                state.form[key] = newValue;
                return state;
            }, ()=>this.hasPassedCustomValidation());
    
        } else{
            this.setState(state=>{
                state.form[key] = newValue;
                return state;
            });    
        }
    }

    hasPassedCustomValidation = () =>{
        let passConfirmation = $('#passwordConfirmation').get(0);
        let pass = $('#password').get(0);
        if(passConfirmation.value !== pass.value){
            passConfirmation.setCustomValidity("Password does not match");
            /*
            passConfirmation.classList.add('invalid');
            passConfirmation.classList.add('is-invalid');
            passConfirmation.classList.remove('valid');
            passConfirmation.classList.remove('is-valid');
            */
            return false;
        }
        else if(passConfirmation.value === ''){
            passConfirmation.setCustomValidity("Password can not be empty");
        } else {
            passConfirmation.setCustomValidity("");
            /*
            passConfirmation.classList.add('valid');
            passConfirmation.classList.add('is-valid');
            passConfirmation.classList.remove('invalid');
            passConfirmation.classList.remove('is-invalid');
            */
        }
        return true;
    }

    onSubmit = (e) => {
        e.preventDefault();
        e.stopPropagation();
        let isFormValid = $('.needs-validation').get(0).checkValidity();
        isFormValid = this.hasPassedCustomValidation() && isFormValid;
        if(isFormValid){
           /*
            to fetch here
            const response = await fetch(url, {
                method: "POST",
                mode: "cors",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(this.state.form)
            });
            */
           // TO SET ERROR MESSAGE FROM SERVER HERE
           // let errMsg = ".....";
           this.setState({isSubmitted: true});
        } else{
            $('.needs-validation').get(0).classList.add('was-validated');
        }
    }

    componentDidMount(){
        this.updatePageHeight();
        window.addEventListener("resize", this.updatePageHeight);
    }

    componentWillUnmount(){
        window.removeEventListener("resize", this.updatePageHeight);
    }
    
    render(){
        let height;
        if(this.state.style.pageHeight > 0){
            height = this.state.style.pageHeight;
        }
        return(
            <>
            <Header/>
            <div className="register-section" style={{minHeight: height}}>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4"></div>
                        <div className="col-lg-4">
                            {this.state.isSubmitted ?
                                <div className="alert alert-success" role="alert">
                                    Successfully Registered
                                </div>
                            :
                            <>
                            { this.state.errorMsg !== "" ?
                            <div className="alert alert-danger" role="alert">
                                {this.state.errorMsg}
                            </div>
                            : null
                            }
                            <h1 className="form-title">Registration Form</h1>
                            <form className="register-form needs-validation" onSubmit={this.onSubmit} noValidate>
                                <div className="form-group">
                                    <label htmlFor="username">Username</label>
                                    <input id="username" type="text"  className="form-control" value={this.state.form.username} onChange={this.onChange} required/>
                                    <div className="valid-feedback"></div>
                                    <div className="invalid-feedback">Please enter username</div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="firstName">First Name</label>
                                    <input id="firstName" type="text" className="form-control" value={this.state.form.firstName} onChange={this.onChange} required/>
                                    <div className="valid-feedback"></div>
                                    <div className="invalid-feedback">Please enter first name</div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="lastName">Last Name</label>
                                    <input id="lastName" type="text" className="form-control" value={this.state.form.lastName} onChange={this.onChange} required/>
                                    <div className="valid-feedback"></div>
                                    <div className="invalid-feedback">Please enter last name</div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="birthDay">Birthday</label>
                                    <input id="birthDay" type="date" className="form-control" value={this.state.form.birthDay} onChange={this.onChange} required/>
                                    <div className="valid-feedback"></div>
                                    <div className="invalid-feedback">Please enter birthday</div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="email">Email</label>
                                    <input id="email" type="email" className="form-control" value={this.state.form.email} onChange={this.onChange} required/>
                                    <div className="valid-feedback"></div>
                                    <div className="invalid-feedback">Please enter valid email</div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <label htmlFor="password">Password</label>
                                        <input id="password" type="password" className="form-control" value={this.state.form.password} onChange={this.onChange} required minLength={6}/>
                                        <div className="valid-feedback"></div>
                                        <div className="invalid-feedback">Please enter password</div>
                                    </div>
                                    <div className="col">
                                        <label htmlFor="passwordConfirmation">Confirm Password</label>
                                        <input id="passwordConfirmation" type="password" className="form-control" value={this.state.form.passwordConfirmation} onChange={this.onChange} minLength={6}/>
                                        <div className="valid-feedback"></div>
                                        <div className="invalid-feedback">Password does not match</div>                                    
                                    </div>
                                </div>
                                <button className="register-btn btn btn-primary btn-block">Register</button>
                            </form>
                            </>
                            }
                        </div>
                        <div className="col-lg-4"></div>
                    </div>                    
                </div>
            </div>
            </>
        );
    }
}

export default Register;

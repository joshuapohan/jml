import React from 'react';
import $ from 'jquery';

import Header from "./common/Header";
import "./NewsDetail.css"

class NewsDetail extends React.Component {
    constructor(props){
        super(props);
        let news = {
            id: 1,
            title: "Detailed news 1",
            content: "Detailed news content"
        }

        /* to fetch here
            fetch(url, {
              method: "GET",
              mode: "cors"
            })
            .then((response)=>{
                response.json();
            })
            .then((news)=>{
                this.setState({
                    news: news
                });
            });
        */
        this.state = {
            news: news,
            style: {
                pageHeight: 0
            }
        };
        this.updatePageHeight = this.updatePageHeight.bind(this);
    }

    updatePageHeight(){
        let winHeight = $(window).height();
        let navHeight = $("#navbar").height();
        let pageHeight;
        if ($(window).width() < 992){
            pageHeight = winHeight - navHeight;
        } else {
            pageHeight = winHeight;
        }
        this.setState({
            style: {
                pageHeight: pageHeight
            }
        });
    }    

    componentDidMount(){
        this.updatePageHeight();
        window.addEventListener("resize", this.updatePageHeight);
    }

    componentWillUnmount(){
        window.removeEventListener("resize", this.updatePageHeight);
    }

    render(){
        let height;
        if(this.state.style.pageHeight > 0){
            height = this.state.style.pageHeight;
        }
        return(
            <>
            <Header/>
            <div style={{minHeight:height}}>
                <h1>Details of post {this.props.match.params.id}</h1>
            </div>
            </>
        );
    }
}

export default NewsDetail;

import React from 'react';
import $ from 'jquery';


import Header from "./common/Header";
import "./Team.css";
import dummy from "../assets/dummy.jpg";

class Team extends React.Component {
    constructor(props){
        super(props);

        // either fetch or hardcode, hardcode for now
        let team = [
            {
                id: 1,
                name: "Member #1",
                position: "Position #1"
            },
            {
                id: 2,
                name: "Member #2",
                position: "Position #2"
            },
            {
                id: 3,
                name: "Member #3",
                position: "Position #3"
            },
            {
                id: 4,
                name: "Member #4",
                position: "Position #4"
            },
            {
                id: 5,
                name: "Member #5",
                position: "Position #5"
            },
            {
                id: 6,
                name: "Member #6",
                position: "Position #6"
            }
        ]

        this.state = {
            team: team,
            style:{
                pageHeight: 0
            }
        };

        this.updatePageHeight = this.updatePageHeight.bind(this);
    }

    updatePageHeight(){
        let winHeight = $(window).height();
        let navHeight = $("#navbar").height();
        let footHeight = $("#footer").height();
        let pageHeight =  winHeight - (navHeight * 2) - footHeight;
        this.setState({
            style: {
                pageHeight: pageHeight
            }
        });
    }    

    componentDidMount(){
        this.updatePageHeight();
        window.addEventListener("resize", this.updatePageHeight);
    }

    componentWillUnmount(){
        window.removeEventListener("resize", this.updatePageHeight);
    }
    
    render(){
        let height;
        if(this.state.style.pageHeight > 0){
            height = this.state.style.pageHeight;
        }
        return(
            <>
            <Header/>
            <div className="team-section" style={{minHeight: height}}>
                <h1 className="team-title">Our Team</h1>
                {this.state.team.map((person)=>{
                    return(
                        <div className="card team-card" style={{width: "20rem"}} key={person.id}>
                            <img src={dummy} className="card-img-top" alt="..."/>
                            <div className="card-body">
                                <h5 className="card-title">{person.name}</h5>
                                <p className="card-text">{person.position}</p>
                            </div>
                        </div>
                    );
                })}
            </div>
            </>
        );
    }
}

export default Team;

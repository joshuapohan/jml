import React from 'react';
import $ from 'jquery';

import Header from "./common/Header";
import MainContent from "./MainContent";
import News from "./NewsSection";
import "./Home.css";


class Home extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            jumboHeight: 0,
            jumboPad: 0
        };
        this.updateJumbotronHeight = this.updateJumbotronHeight.bind(this);
    }

    updateJumbotronHeight(){
        let winHeight = $(window).height();
        //let navHeight = $("#navbar").height();
        let jumboHeight = winHeight;
        this.setState({
            jumboHeight: jumboHeight,
            jumboPad: (jumboHeight) / 3.2
        });
    }    

    componentDidMount(){
        this.updateJumbotronHeight();
        window.addEventListener("resize", this.updateJumbotronHeight);
    }

    componentWillUnmount(){
        window.removeEventListener("resize", this.updateJumbotronHeight);
    }

    render(){
        let jumboStyle = {};
        if(this.state.jumboHeight > 0 && this.state.jumboPad > 0){
            jumboStyle = {
                height: this.state.jumboHeight,
                paddingTop: this.state.jumboPad
            };
        }
        return(
            <>
                <Header/>
                <div className="container-fluid main-content">
                    <div className="jumbotron" style={jumboStyle}>
                        {/* <img src={logo} style={{width: "300px"}} alt="Jakarta Machine Learning"/> */}
                        <h1 style={{fontSize:"10rem"}}>JML</h1>
                        <h2>Community</h2>
                        <h1>"Learning is the new cool"</h1>
                    </div>
                </div>
                <News/>
                <MainContent/>
            </>
        );
    }
}

export default Home;

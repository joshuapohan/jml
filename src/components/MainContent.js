import React from 'react';
import $ from 'jquery';

import './MainContent.css'

class MainContent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            pageHeight: 0
        };
        this.updatePageHeight = this.updatePageHeight.bind(this);
    }

    updatePageHeight(){
        let winHeight = $(window).height();
        let navHeight = $("#navbar").height();
        let pageHeight;
        if ($(window).width() < 992){
            pageHeight = winHeight - navHeight;
        } else {
            pageHeight = winHeight;
        }
        this.setState({
            pageHeight: pageHeight
        });
    }    

    componentDidMount(){
        this.updatePageHeight();
        window.addEventListener("resize", this.updatePageHeight);
    }

    componentWillUnmount(){
        window.removeEventListener("resize", this.updatePageHeight);
    }
    render(){
        let style ={};
        if(this.state.pageHeight > 0){
            style = {
                minHeight: this.state.pageHeight
            }
        }
        return(
            <div className="main" style={style}>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-6 content-bg" style={{minHeight:this.state.pageHeight}}></div>
                        <div className="col-lg-6 content-section" style={{minHeight:this.state.pageHeight}}>
                        <span className="glyphicon glyphicon-search" aria-hidden="true"></span><h1 className="section-title">About Us</h1>
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
                            <h1 className="section-title">What we do</h1>      
                            <div className="content-section">
                                <p>1. Machine Learning training for beginners</p>
                                <p>2. Focus Group Discussion to solve available data online (Kaggle, UCL, Stanford, etc)</p>
                                <p>3. Sharing knowledge about cutting edge AI implementation in the world</p>                            
                            </div>        
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MainContent;

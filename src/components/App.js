import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Footer from "./common/Footer";
import Home from "./Home";
import NewsPage from "./NewsPage";
import NewsDetail from "./NewsDetail";
import Team from "./Team";
import Register from "./Register";

class App extends React.Component {
    render(){
        return(
            <BrowserRouter>
                <Switch>
                    <Route path="/" exact component={Home}/>
                </Switch>
                <Switch>
                    <Route path="/news" component={NewsPage}/>
                </Switch>
                <Switch>
                    <Route path="/details/:id" component={NewsDetail}/>
                </Switch>
                <Switch>
                    <Route path="/team" exact component={Team}/>
                </Switch>
                <Switch>
                    <Route path="/register" exact component={Register}/>
                </Switch>
                <Footer/>
            </BrowserRouter>
        );
    }
}

export default App;

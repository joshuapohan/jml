import React from 'react';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import {withRouter} from 'react-router-dom';

import logo from '../../assets/logo.png'
import './Header.css'

const defaultNavClasses = "navbar navbar-expand-lg navbar-light fixed-top ";

class Header extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            navClasses: defaultNavClasses
        };
        this.updateNavBarStyle = this.updateNavBarStyle.bind(this);
    }

    updateNavBarStyle = () => {
        let navClasses = defaultNavClasses;
        if($(window).width() < 992){
            navClasses += "navbar-white";
        } else {
            if($(window).scrollTop() === 0 && $("body").height() >= $(window).height() && this.props.location.pathname === "/"){
                navClasses += "navbar-transparent";
            } else {
                navClasses += "navbar-white";
            }
        }
        this.setState({
            navClasses: navClasses
        });
    }    

    componentDidMount(){
        this.updateNavBarStyle();
        window.addEventListener("resize", this.updateNavBarStyle);
        window.addEventListener("scroll", this.updateNavBarStyle);
    }

    componentWillUnmount(){
        window.removeEventListener("resize", this.updateNavBarStyle);
        window.removeEventListener("scroll", this.updateNavBarStyle);
    }
    
    render(){
        return(
            <nav id="navbar" className={this.state.navClasses}>
                <img src={logo} width="198" height="65" alt=""/>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <Link to="/" className="nav-link">Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/news" className="nav-link">News</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/team" className="nav-link">Our Team</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/register" className="nav-link">Register</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default withRouter(Header);

import React from 'react';

import "./Footer.css"

class Footer extends React.Component {
    render(){
        return(
            <footer id="footer" className="section footer-classic context-dark bg-image">
                <div className="container-fluid">
                  <div className="row row-30">
                    <div className="col-md-4 col-xl-5">
                      <div className="pr-xl-4">
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."</p>
                        <p><span>©  </span><span>2020</span><span> </span><span>Jakarta Machine Learning</span><span>. </span><span>All Rights Reserved.</span></p>
                      </div>
                    </div>
                    <div className="col-md-4">
                    </div>
                    <div className="col-md-4 col-xl-3">
                        <h5>Contacts</h5>
                        <dl>
                            <dt>email:</dt>
                            <dd><a href="mailto:#">test@test.com</a></dd>
                        </dl>
                        <dl>
                            <dt>phones:</dt>
                            <dd>
                                phone number
                            </dd>
                        </dl>
                    </div>
                  </div>
                </div>
            </footer>
        );
    }
}

export default Footer;

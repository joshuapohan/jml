import React from 'react';
import $ from 'jquery';
import { Link } from 'react-router-dom';

import Header from "./common/Header";
import "./NewsPage.css";
import dummy from "../assets/placeholder-image.png";

class NewsPage extends React.Component {
    constructor(props){
        super(props);

        let dummyNews = [{
            id: 0,
            title: "Title #1",
            content: "Content #1",
            date: "1990-01-01"
        },
        {
            id: 1,
            title: "Title #1",
            content: "Content #1",
            date: "1990-01-01"
        },
        {
            id: 2,
            title: "Title #1",
            content: "Content #1",
            date: "1990-01-01"
        },
        {
            id: 3,
            title: "Title #1",
            content: "Content #1",
            date: "1990-01-01"
        },
        {
            id: 4,
            title: "Title #1",
            content: "Content #1",
            date: "1990-01-01"
        },
        {
            id: 5,
            title: "Title #1",
            content: "Content #1",
            date: "1990-01-01"
        },
        {
            id: 6,
            title: "Title #1",
            content: "Content #1",
            date: "1990-01-01"
        },
        ];

        this.state = {
            news: dummyNews,
            pageInfo: {
                currentPage: 0,
                maxPage: 0
            },
            style: {
                pageHeight: 0
            }
        };
        this.updatePageHeight = this.updatePageHeight.bind(this);
    }

    updatePageHeight(){
        let winHeight = $(window).height();
        let navHeight = $("#navbar").height();
        let pageHeight;
        if ($(window).width() < 992){
            pageHeight = winHeight - navHeight;
        } else {
            pageHeight = winHeight;
        }
        this.setState({
            style: {
                pageHeight: pageHeight
            }
        });
    }    

    componentDidMount(){
        this.updatePageHeight();
        window.addEventListener("resize", this.updatePageHeight);
    }

    componentWillUnmount(){
        window.removeEventListener("resize", this.updatePageHeight);
    }

    render(){
        let height;
        if(this.state.style.pageHeight > 0){
            height = this.state.style.pageHeight;
        }
        if(this.state.news != null){
            return(
                <>
                <Header/>
                <div style={{minHeight: height}}>
                    <div>
                        <h1 className="news-page-title">News</h1>
                    </div>
                    <div className="news-page">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-sm-1"></div>
                                <div className="col-sm-10">
                                    {   this.state.news.map((news)=>{
                                            return(                                                
                                                <div className="news-page-card" key={news.id}>
                                                    <img src={dummy} className="card-img-top" alt="..."/>
                                                    <div className="card-body">
                                                        <h5 className="card-title">{news.title}</h5>
                                                        <p className="card-text">{news.content}</p>
                                                        <Link to={"/details/" + news.id} className="btn btn-primary btn-block">Details</Link>
                                                    </div>
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                                <div className="col-sm-1"></div>
                            </div>    
                        </div>
                    </div>
                </div>
                </>
            );
        } else {
            return(
                <>
                <Header/>
                <div style={{height:800}} className="news">
                    Loading
                </div>
                </>
            );
        }
    }
}

export default NewsPage;
import React from 'react';
import {Link} from 'react-router-dom';
import $ from 'jquery';

import "./NewsSection.css";
import dummy from "../assets/placeholder-image.png";

class News extends React.Component {
    constructor(props){
        super(props);
        let news = [{
            id: 1,
            title: "News number one",
            content: "Content number one"
        },
        {
            id: 2,
            title: "News number two",
            content: "Content number two"
        },
        {
            id: 3,
            title: "News number three",
            content: "Content number three"
        }];

        /*
            to fetch here
            fetch(url, {
              method: "GET",
              mode: "cors"
            })
            .then((response)=>{
                response.json();
            })
            .then((news)=>{
                this.setState({
                    news: news
                });
            });
        */
        this.state = {
            news: news,
            pageHeight: 0
        };
        this.updatePageHeight = this.updatePageHeight.bind(this);
    }

    updatePageHeight(){
        let winHeight = $(window).height();
        let navHeight = $("#navbar").height();
        let pageHeight;
        if ($(window).width() < 992){
            pageHeight = winHeight - navHeight;
        } else {
            pageHeight = winHeight;
        }
        this.setState({
            pageHeight: pageHeight
        });
    }    

    componentDidMount(){
        this.updatePageHeight();
        window.addEventListener("resize", this.updatePageHeight);
    }

    componentWillUnmount(){
        window.removeEventListener("resize", this.updatePageHeight);
    }

    render(){
        let height;
        if(this.state.pageHeight > 0){
            height = this.state.pageHeight;
        }
        if(this.state.news != null){
            return(
                <div className="news-section" style={{minHeight: height}}>
                    <div>
                        <h1 className="section-title">News</h1>
                    </div>
                    <div className="news">
                        { this.state.news.map((news)=>{
                                return(
                                    <div className="card" style={{width: "20rem"}} key={news.id}>
                                        <img src={dummy} className="card-img-top" alt="..."/>
                                        <div className="card-body">
                                            <h5 className="card-title">{news.title}</h5>
                                            <p className="card-text">{news.content}</p>
                                            <Link to={"/details/" + news.id} className="btn btn-primary btn-block">Details</Link>
                                        </div>
                                    </div>
                                );
                            })
                        }
                    </div>
                </div>
            );
        } else {
            return(
                <div style={{height:800}} className="news">
                    Loading
                </div>
            );
        }
    }
}

export default News;
